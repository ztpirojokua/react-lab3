import React from "react";
import {Box, Modal} from "@mui/material";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  border: "1px solid #00f",
  borderRadius: "1px",
  boxShadow: 5,
  p: 2,
};

const PicturesThumbnail = ({visible, setVisible, picture}) => {
  const handleClose = () => setVisible(false);

  return (
    <div>
      <Modal
        open={visible}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <img
            style={{maxWidth: "100%", maxHeight: "calc(100vh - 64px)"}}
            alt="picture"
            src={picture}
          />
        </Box>
      </Modal>
    </div>
  );
};

export default PicturesThumbnail;
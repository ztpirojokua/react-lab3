import React, { useEffect, useState } from "react";
import axios from "axios";
import { CircularProgress } from "@mui/material";
import PicturesVirtualizedTable from "./PicturesVirtualizedTable";

const PicturesContainer = () => {
  const [pictures, setPictures] = useState();

  const fetchPictures = () => {
    let url = "https://jsonplaceholder.typicode.com/photos";

    axios.get(url).then(response => {
      if (response.status === 200) {
        setPictures(response.data?.filter(picture => picture.title.split(" ").length <= 7));
      }
    }).catch(error => {
      console.log(error.response?.data);
    });
  };

  useEffect(() => {
    fetchPictures();
  }, []);

  if (!pictures) {
    return <CircularProgress color="secondary" sx={{position: "absolute", top: "50%", left: "50%"}} />;
  }

  return (
    <div>
      <PicturesVirtualizedTable pictures={pictures} />
    </div>
  );
};

export default PicturesContainer;

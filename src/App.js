import "./App.css";
import PicturesContainer from "./components/picture/PicturesContainer";

const App = () => {
  return (
    <div className="App">
        <PicturesContainer />
    </div>
  );
};

export default App;
